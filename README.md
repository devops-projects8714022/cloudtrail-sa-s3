### Документация по настройке AWS CloudTrail для сбора логов со всех учетных записей организации AWS и их сохранения в S3-бакете учетной записи безопасности с использованием Terraform

#### Шаг 1: Создание S3-бакета в учетной записи безопасности

1. **Создайте S3-бакет для хранения логов CloudTrail.**
2. **Примените политики бакета, чтобы разрешить доступ ко всем учетным записям в организации.**

Пример кода Terraform:

```hcl
provider "aws" {
  alias  = "security_account"
  region = "us-east-1"
  profile = "security_account_profile" # Укажите профиль учетной записи безопасности
}

resource "aws_s3_bucket" "cloudtrail_bucket" {
  provider = aws.security_account
  bucket   = "your-cloudtrail-logs-bucket"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "retain-logs"
    enabled = true

    noncurrent_version_expiration {
      days = 365
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

resource "aws_s3_bucket_policy" "cloudtrail_bucket_policy" {
  provider = aws.security_account
  bucket   = aws_s3_bucket.cloudtrail_bucket.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow CloudTrail logs",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Sid": "Allow Access from Organization Accounts",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "aws:PrincipalOrgID": "o-xxxxxxxxxx"
        }
      }
    }
  ]
}
POLICY
}
```

#### Шаг 2: Настройка CloudTrail в каждой учетной записи

1. **Используйте источник данных `aws_organizations_organization`, чтобы получить список всех учетных записей в организации.**
2. **Настройте CloudTrail в каждой учетной записи для отправки логов в S3-бакет учетной записи безопасности.**

Пример кода Terraform:

```hcl
provider "aws" {
  alias  = "management_account"
  region = "us-east-1"
  profile = "management_account_profile" # Укажите профиль учетной записи управления
}

data "aws_organizations_organization" "org" {
  provider = aws.management_account
}

resource "aws_cloudtrail" "org_trail" {
  for_each = toset(data.aws_organizations_organization.org.accounts.*.id)

  name                          = "organization-trail-${each.value}"
  s3_bucket_name                = aws_s3_bucket.cloudtrail_bucket.id
  include_global_service_events = true
  is_multi_region_trail         = true
  enable_log_file_validation    = true
  is_organization_trail         = true

  depends_on = [
    aws_s3_bucket_policy.cloudtrail_bucket_policy
  ]
}
```

#### Шаг 3: IAM роли и политики

1. **Создайте IAM роль с необходимыми разрешениями для разрешения CloudTrail записывать логи в S3-бакет.**
2. **Убедитесь, что эта IAM роль может быть использована сервисом CloudTrail из всех учетных записей организации.**

Пример кода Terraform:

```hcl
resource "aws_iam_role" "cloudtrail_role" {
  name = "CloudTrail_Logging_Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cloudtrail_policy" {
  name        = "CloudTrail_Logging_Policy"
  description = "Policy for CloudTrail to log to S3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_cloudtrail_policy" {
  role       = aws_iam_role.cloudtrail_role.name
  policy_arn = aws_iam_policy.cloudtrail_policy.arn
}
```

### Полный пример конфигурации Terraform

Объедините все части в единую конфигурацию Terraform. Убедитесь, что вы корректно настроили профили, регионы и другие специфические параметры в соответствии с вашей настройкой AWS.

```hcl
# Провайдер для учетной записи безопасности
provider "aws" {
  alias  = "security_account"
  region = "us-east-1"
  profile = "security_account_profile"
}

# Провайдер для учетной записи управления
provider "aws" {
  alias  = "management_account"
  region = "us-east-1"
  profile = "management_account_profile"
}

# Создание S3 бакета в учетной записи безопасности
resource "aws_s3_bucket" "cloudtrail_bucket" {
  provider = aws.security_account
  bucket   = "your-cloudtrail-logs-bucket"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "retain-logs"
    enabled = true

    noncurrent_version_expiration {
      days = 365
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

# Применение политики бакета
resource "aws_s3_bucket_policy" "cloudtrail_bucket_policy" {
  provider = aws.security_account
  bucket   = aws_s3_bucket.cloudtrail_bucket.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow CloudTrail logs",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Sid": "Allow Access from Organization Accounts",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "aws:PrincipalOrgID": "${data.aws_organizations_organization.org.id}"
        }
      }
    }
  ]
}
POLICY
}

# Получение учетных записей организации
data "aws_organizations_organization" "org" {
  provider = aws.management_account
}

# Создание CloudTrail в каждой учетной записи
resource "aws_cloudtrail" "org_trail" {
  for_each = toset(data.aws_organizations_organization.org.accounts.*.id)

  name                          = "organization-trail-${each.value}"
  s3_bucket_name                = aws_s3_bucket.cloudtrail_bucket.id
  include_global_service_events = true
  is_multi_region_trail         = true
  enable_log_file_validation    = true
  is_organization_trail         = true

  depends_on = [
    aws_s3_bucket_policy.cloudtrail_bucket_policy
  ]
}

# IAM роль и политики для записи логов CloudTrail в S3 бакет
resource "aws_iam_role" "cloudtrail_role" {
  provider = aws.security_account
  name     = "CloudTrail_Logging_Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cloudtrail_policy" {
  provider = aws.security_account
  name        = "CloudTrail_Logging_Policy"
  description = "Policy for CloudTrail to log to S3"

  policy = <<EOF
{
  "Version": "2012-10-

17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_cloudtrail_policy" {
  provider = aws.security_account
  role       = aws_iam_role.cloudtrail_role.name
  policy_arn = aws_iam_policy.cloudtrail_policy.arn
}
```

### Примечания:
- Замените `your-cloudtrail-logs-bucket` на желаемое имя бакета.
- Убедитесь, что профили и регионы соответствуют вашей настройке AWS.
- Убедитесь, что ваш AWS CLI настроен правильно для управления несколькими учетными записями.

Этот конфигурационный файл Terraform настраивает S3-бакет в учетной записи безопасности, применяет необходимые политики бакета и настраивает CloudTrail в каждой учетной записи для отправки логов в S3-бакет.