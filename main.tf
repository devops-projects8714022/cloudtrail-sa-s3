# Provider for Security Account
provider "aws" {
  alias  = "security_account"
  region = "us-east-1"
  profile = "security_account_profile"
}

# Provider for Management Account
provider "aws" {
  alias  = "management_account"
  region = "us-east-1"
  profile = "management_account_profile"
}

# Create S3 bucket in the Security Account
resource "aws_s3_bucket" "cloudtrail_bucket" {
  provider = aws.security_account
  bucket   = "your-cloudtrail-logs-bucket"

  versioning {
    enabled = true
  }

  lifecycle_rule {
    id      = "retain-logs"
    enabled = true

    noncurrent_version_expiration {
      days = 365
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
}

# Apply bucket policy
resource "aws_s3_bucket_policy" "cloudtrail_bucket_policy" {
  provider = aws.security_account
  bucket   = aws_s3_bucket.cloudtrail_bucket.id

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "Allow CloudTrail logs",
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    },
    {
      "Sid": "Allow Access from Organization Accounts",
      "Effect": "Allow",
      "Principal": "*",
      "Action": "s3:PutObject",
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "aws:PrincipalOrgID": "${data.aws_organizations_organization.org.id}"
        }
      }
    }
  ]
}
POLICY
}

# Fetch Organization Accounts
data "aws_organizations_organization" "org" {
  provider = aws.management_account
}

# Create CloudTrail in each account
resource "aws_cloudtrail" "org_trail" {
  for_each = toset(data.aws_organizations_organization.org.accounts.*.id)

  name                          = "organization-trail-${each.value}"
  s3_bucket_name                = aws_s3_bucket.cloudtrail_bucket.id
  include_global_service_events = true
  is_multi_region_trail         = true
  enable_log_file_validation    = true
  is_organization_trail         = true

  depends_on = [
    aws_s3_bucket_policy.cloudtrail_bucket_policy
  ]
}

# IAM Role and Policies for CloudTrail to write to S3 bucket
resource "aws_iam_role" "cloudtrail_role" {
  provider = aws.security_account
  name     = "CloudTrail_Logging_Role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "cloudtrail.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_policy" "cloudtrail_policy" {
  provider = aws.security_account
  name        = "CloudTrail_Logging_Policy"
  description = "Policy for CloudTrail to log to S3"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": "arn:aws:s3:::${aws_s3_bucket.cloudtrail_bucket.id}/AWSLogs/*",
      "Condition": {
        "StringEquals": {
          "s3:x-amz-acl": "bucket-owner-full-control"
        }
      }
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "attach_cloudtrail_policy" {
  provider = aws.security_account
  role       = aws_iam_role.cloudtrail_role.name
  policy_arn = aws_iam_policy.cloudtrail_policy.arn
}
